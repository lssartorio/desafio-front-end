# Quer trabalhar em nosso time de frontend? #

Esse é o nosso desafio para você!
O objetivo é avaliar o seu conhecimento e habilidades. O desenvolvimento deve seguir fielmente o layout disponibilizado (espaçamento, cores, tipografia e etc). Você não deve utilizar nenhum framework CSS (Bootstrap, Foundation e etc) neste desafio. Você pode usar libs e frameworks como Jquery, AngularJS ou Angular 5 para manipular os elementos, entretando, não pode ser utilizado nenhum tipo de plugin.

## Instruções ##

* O layout proposto é do tipo one page, com links âncoras;
* O menu da página deve permanecer fixo no scroll da página e os links devem estar ancorados nas respectivas seções;
* Desenvolva a página seguindo os layouts apresentados no diretório "layout" (existem versões em PSD e JPG);
* O layout deve ser responsivo em apenas dois breakpoints: desktop e mobile. O ajuste do layout mobile fica a seu critério;
* Faça uma requisição para o arquivo JSON/servicos.json para ler os dados da seção "Serviços";
    * O estado inicial das informações na seção exibe somente o título;
    * No evento de *hover*, o respectivo texto de cada *box* deve ser exibido, conforme [imagem exemplo](Layout/layout_hover_secao_servicos.jpg);
* Você está livre para escolher a estrutura de diretórios da aplicação;
* O uso de pré-processadores de CSS e task runners será muito bem vindo!

## O que será avaliado ##

* Markup
* CSS
* Javascript
* Web Standards
* Semântica
* SEO
* Performance
* Organização

## Arquivos ##
- Layout [aqui](Layout)
- JSON [aqui](JSON)

## Como submeter seu projeto ##
* Fork esse projeto;
* Coloque o projeto no seu repositório;
* Escreva no arquivo README.md as instruções para rodar o mesmo;
* Envie o link do repositório para selecao@genialinvestimentos.com.br, com o assunto: TESTE - Front-End Genial.